# Single likelihood evaluation took 1.429e-01 s with 8 processes
import numpy as np
import bilby

C = 299792458.
G = 6.67408*1e-11
Mo = 1.989*1e30
Mpc = 3.086*1e22

outdir = 'outdir_TaylorF2EccPerAdvFDLeOLowMass'
label = 'TaylorF2EccPerAdvFDLeOLowMass'
bilby.core.utils.setup_logger(outdir=outdir, label=label)

time_of_event = 1268431074.1
post_trigger_duration = 1.0
duration = 264.
analysis_start = time_of_event + post_trigger_duration - duration
sampling_frequency = 512.

# frequency_array, mass_1, mass_2, luminosity_distance, a_1, a_2, theta_jn, phase
# chirp_mass = 8.74158695118774, mass_ratio = 0.5954198473282443
# total snr=50.35922022341398, h1,l1,v1=32.78421559764631,26.64206447587012,27.41252760365106
injection_parameters = dict(
    mass_1=1.46, mass_2=1.27, eccentricity=0.08, luminosity_distance=57.,
    theta_jn=0.2, psi=2.659, phase=1.3, geocent_time=1268431074.1, ra=1.375, dec=-1.2108)

waveform_arguments = dict(waveform_approximant='TaylorF2EccPerAdvFD1PNAmpLeO',
                          reference_frequency=20., minimum_frequency=20., catch_waveform_errors=True)

waveform_arguments2 = dict(waveform_approximant='TaylorF2EccPerAdvFD1PNAmp',
                          reference_frequency=20., minimum_frequency=20., catch_waveform_errors=True)

waveform_generator = bilby.gw.WaveformGenerator(
    duration=duration, sampling_frequency=sampling_frequency,
    frequency_domain_source_model=bilby.gw.source.lal_TaylorF2EccPerAdvFD1PNAmpLeO,
    waveform_arguments=waveform_arguments)

waveform_generator2 = bilby.gw.WaveformGenerator(
    duration=duration, sampling_frequency=sampling_frequency,
    frequency_domain_source_model=bilby.gw.source.lal_TaylorF2EccPerAdvFD1PNAmp,
    waveform_arguments=waveform_arguments2)

minimum_frequency = 20.
maximum_frequency = 256.

ifos = bilby.gw.detector.InterferometerList(['H1', 'L1', 'V1'])
for ifo in ifos:
    ifo.minimum_frequency = minimum_frequency
    ifo.maximum_frequency = maximum_frequency
ifos.set_strain_data_from_power_spectral_densities(
    sampling_frequency=sampling_frequency, duration=duration,
    start_time=analysis_start)
ifos.inject_signal(waveform_generator=waveform_generator2,
                   parameters=injection_parameters)


'''injection_parameters = dict(
    mass_1=1.46, mass_2=1.27, eccentricity=0.08, luminosity_distance=57.,
    theta_jn=0.2, psi=2.659, phase=1.3, geocent_time=1268431074.1, ra=1.375, dec=-1.2108)'''
prior = bilby.core.prior.PriorDict()
prior['mass_1'] = bilby.core.prior.Constraint(name='mass_1', minimum=1.1,maximum=2.0)
prior['mass_2'] = bilby.core.prior.base.Constraint(name='mass_2', minimum=1.1,maximum=2.0)
prior['chirp_mass'] = bilby.gw.prior.Uniform(name='chirp_mass', minimum=0.9576056196257366,maximum=1.7411011265922482)
prior['mass_ratio'] = bilby.gw.prior.Uniform(name='mass_ratio', minimum=0.55, maximum=1.)
prior['eccentricity'] = bilby.gw.prior.Uniform(name='e0', minimum=0.0, maximum=0.1)
prior["luminosity_distance"] = 57.
prior["theta_jn"] = 0.2
prior["psi"] = 2.659
prior["phase"] = bilby.gw.prior.Uniform(name='phase', minimum=0.0, maximum=3.14)
prior["geocent_time"] = 1268431074.1
prior["ra"] = 1.375
prior["dec"] = -1.2108

likelihood = bilby.gw.likelihood.GravitationalWaveTransient(
    interferometers=ifos, waveform_generator=waveform_generator, priors=prior)

result_short = bilby.core.sampler.run_sampler(
    likelihood, prior, sampler='dynesty', outdir=outdir, label=label,
    nlive=500, dlogz=0.1, npool=8, resume=True )
