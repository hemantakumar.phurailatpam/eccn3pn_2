#!/usr/bin/env bash

# InectionStudyTaylorF2Ecc15to32Hz_data0_1126259642-413_generation
# PARENTS 
# CHILDREN InectionStudyTaylorF2Ecc15to32Hz_data0_1126259642-413_analysis_H1L1
/home/hemantakumar.phurailatpam/anaconda3/envs/lal2/bin/bilby_pipe_generation outdir_InectionStudyTaylorF2Ecc15to32Hz/InectionStudyTaylorF2Ecc15to32Hz_config_complete.ini --label InectionStudyTaylorF2Ecc15to32Hz_data0_1126259642-413_generation --idx 0 --trigger-time 1126259642.413

# InectionStudyTaylorF2Ecc15to32Hz_data0_1126259642-413_analysis_H1L1
# PARENTS InectionStudyTaylorF2Ecc15to32Hz_data0_1126259642-413_generation
# CHILDREN InectionStudyTaylorF2Ecc15to32Hz_data0_1126259642-413_analysis_H1L1_final_result
/home/hemantakumar.phurailatpam/anaconda3/envs/lal2/bin/bilby_pipe_analysis outdir_InectionStudyTaylorF2Ecc15to32Hz/InectionStudyTaylorF2Ecc15to32Hz_config_complete.ini --outdir outdir_InectionStudyTaylorF2Ecc15to32Hz --detectors H1 --detectors L1 --label InectionStudyTaylorF2Ecc15to32Hz_data0_1126259642-413_analysis_H1L1 --data-dump-file outdir_InectionStudyTaylorF2Ecc15to32Hz/data/InectionStudyTaylorF2Ecc15to32Hz_data0_1126259642-413_generation_data_dump.pickle --sampler dynesty

# InectionStudyTaylorF2Ecc15to32Hz_data0_1126259642-413_analysis_H1L1_final_result
# PARENTS InectionStudyTaylorF2Ecc15to32Hz_data0_1126259642-413_analysis_H1L1
# CHILDREN 
/home/hemantakumar.phurailatpam/anaconda3/envs/lal2/bin/bilby_result --result outdir_InectionStudyTaylorF2Ecc15to32Hz/result/InectionStudyTaylorF2Ecc15to32Hz_data0_1126259642-413_analysis_H1L1_result.hdf5 --outdir outdir_InectionStudyTaylorF2Ecc15to32Hz/final_result --extension hdf5 --max-samples 20000 --lightweight --save

