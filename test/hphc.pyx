cimport cython
import numpy as np
cimport numpy as np

from libc.math cimport pow, M_PI

cdef double C =  299792458.0, G = 6.67408e-11, Mo = 1.989*1e30, Mpc = 3.086e22

cdef extern from "complex.h" nogil:
    double complex I
    double creal(double complex)
    double cimag(double complex)

cdef extern from "hphc.h":
    void core( freq *freq_info, arg2 *arg, hphc_p *hphc_values)
    
    ctypedef struct hphc_p:
        double complex *hp
        double complex *hc
    
    ctypedef struct arg2:
        double M
        double eta
        double delta
        double et0
        double D
        double iota
        double beta
        double phic
        double tc
        double f0
        double ff

    ctypedef struct freq:
        double *freq_arr
        int *freq_idx
        size_t idx_len

    ctypedef struct test_arg:
        double *k
    

def eccentric_waveform( np.ndarray[double, ndim=1, mode="c"] frequency_array_, double chirp_mass, double mass_ratio, double initial_eccentricity, double min_frequency, double max_frequency, double luminosity_distance, double theta_jn, double psi, double phase ):

    cdef:
        double Mc = chirp_mass
        double q = mass_ratio
    
        double mass_1 = ( Mc*pow(1.0+q,1./5.) )/pow(q,3./5.)
        double mass_2 = Mc*pow(q,2./5.)*pow(1.0+q,1./5.)

    cdef arg2 arg
    arg.M = (mass_1+mass_2)*Mo
    arg.eta = (mass_1*mass_2)/pow(mass_1+mass_2,2.)
    arg.delta = (mass_1-mass_2)*Mo
    arg.et0 = initial_eccentricity
    arg.D = luminosity_distance*Mpc
    arg.iota = theta_jn
    arg.beta = psi
    arg.phic = phase
    arg.tc = 0.0
    arg.f0 = min_frequency
    arg.ff = (C*C*C)/( G*(mass_1+mass_2)*Mo*M_PI*pow(6,3./2.) )
    
    cdef size_t len_arr = len(frequency_array_)
    cdef np.ndarray[double complex, ndim=1, mode="c"] hp_ = np.zeros( len_arr, np.cdouble)
    cdef np.ndarray[double complex, ndim=1, mode="c"] hc_ = np.zeros( len_arr, np.cdouble)
    cdef hphc_p hphc_values

    hphc_values.hp = <double complex *> hp_.data
    hphc_values.hc = <double complex *> hc_.data

    cdef np.ndarray[int , ndim=1, mode="c"] index = np.array(np.where( (frequency_array_ >= min_frequency) & (frequency_array_ <= arg.ff) ), dtype='int32').flatten()
    cdef freq freq_info
    freq_info.freq_arr = <double *> frequency_array_.data
    freq_info.freq_idx = <int *> index.data
    freq_info.idx_len = <size_t> len(index)

    core( &freq_info, &arg, &hphc_values)

    #return {'plus':hp_, 'cross':hc_ }
    return ( hp_, hc_ )