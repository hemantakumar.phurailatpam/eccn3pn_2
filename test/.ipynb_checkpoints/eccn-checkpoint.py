# written by Hemanta Phurailatpam (CUHK)
# python code for parameter estimation with eccentric waveform model (accuracy: 0PN at amplitude, 3PN et0^6 at fourier phase)

import bilby
import numpy as np
# to print out the time of execution
import time 

# defining some useful constant
C = 299792458.
G = 6.67408*1e-11
Mo = 1.989*1e30
Mpc = 3.086*1e22

# duration is chosen according to the prior massses
duration = 6.
# sampling_frequency is chosen according to the amount of frequency information needed and also the speed of sampling
sampling_frequency = 512.


# chirp_mass = ( (mass_1*mass_2)**(3/5) )/( (mass_1+mass_2)**(1/5) )
# mass_ratio = mass_2/mass_1
injection_parameters = dict(
    mass_1=13.5, mass_2=11.5, eccentricity=0.1, luminosity_distance=200.,
    theta_jn=0.4, psi=0.1, phase=1.2, geocent_time=1180002601.0, ra=45, dec=5.73)

waveform_arguments = dict(waveform_approximant='OSMFD',
                          reference_frequency=20., minimum_frequency=20.)

# Create the waveform_generator using the lal_eccentric_advancement_of_pariastron from source.py (bilby/gw)
waveform_generator = bilby.gw.WaveformGenerator(
    duration=duration, sampling_frequency=sampling_frequency,
    frequency_domain_source_model=bilby.gw.source.lal_eccentric_advancement_of_pariastron,
    parameters=injection_parameters, waveform_arguments=waveform_arguments)

# f_min is set acording to the how much noise is present at lower frequency
# it is more likely to see higher eccentricity at f_min=10.0
# eccentricity die down fast and orbit circularize
minimum_frequency = 20.0
#f_max is set according to the priors we choose for chirp_mass and mass_ratio; depends on max possible lso_f. 
maximum_frequency = 200.0

# injection start-time is set according to range of prior masses and duration chosen 
# you don't want the inspiral part not go out of this chosen duration
ifos = bilby.gw.detector.InterferometerList(['H1', 'L1'])
for ifo in ifos:
    ifo.minimum_frequency = minimum_frequency
    ifo.maximum_frequency = maximum_frequency
ifos.set_strain_data_from_power_spectral_densities(
    sampling_frequency=sampling_frequency, duration=duration,
    start_time=injection_parameters['geocent_time'] - 5.0)
ifos.inject_signal(waveform_generator=waveform_generator,
                   parameters=injection_parameters)

prior = bilby.core.prior.PriorDict()
prior['chirp_mass'] = bilby.core.prior.Uniform(name='chirp_mass', minimum=5.0,maximum=20.0)
prior['mass_ratio'] = bilby.core.prior.Uniform(name='mass_ratio', minimum=0.5, maximum=1)
prior['eccentricity'] = bilby.core.prior.LogUniform(name='eccentricity', minimum=0.01, maximum=0.2)
prior["luminosity_distance"] = 200.0
prior["theta_jn"] = 0.4
prior["psi"] = 0.1
prior["phase"] = 1.2
prior["geocent_time"] = 1180002601.0
prior["ra"] = 45.0
prior["dec"] = 5.73

#this is to run the code faster
likelihood = bilby.gw.likelihood.GravitationalWaveTransient(
    interferometers=ifos, waveform_generator=waveform_generator, priors=prior)

t0 = time.time()

result_short = bilby.core.sampler.run_sampler(
    likelihood, prior, sampler='dynesty', outdir='short', label="eccn",
    nlive=500, dlogz=3.0, npool=8 )

t1 = time.time()
print('Execution took {:.3f}'.format(t1 - t0))
























